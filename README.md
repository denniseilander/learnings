# learnings

This project contains applied knowledge of what I have learned.
For example:
- Build a module and build tests for it.
- Make use of new css framework Tailwind
- Make use of code linters
- Build an autodeploy script
- Build hooks for git